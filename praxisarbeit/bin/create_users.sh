#!/bin/bash
# Script:    script for creating new users
while getopts f: optvar;
do
    case $varforopt in f) filename="${OPTARG}" ;; esac
done . /log.sh

grep -v '^#'| grep -v '^$'|
while read username groupname full_name
do
        username=$username, groupname=$groupname, full_name=$full_name

        #username wird erstellt
        function existing_users () {
            if id $username >/dev/null 2<&1; then
                echo "This User already exists: $username"
                existing_home_directory
            else
                echo "A new User is created: $username"
                sudo useradd -g $groupname -m -k /etc/skel/$groupname -c "$full_name" -p "rootpw" $username
                User log: created $username, username gets group: $groupname, skeleton directory structure: /etc/skel/$groupname, Full name: "$full_name"
            fi
        }
        #skeleton umgebung
        function directory_struc () {
            if [ -d "/etc/skel/$groupname" ]; then
                echo "Existing Skel structure for: $groupname"
                existing_users
            else
                echo "No skel structure found for: $groupname"
                echo "Created new User: $username"
                sudo useradd -g $groupname -m -c "$full_name" -p "rootpw" $username
                User log: created $username, username gets group: $groupname, Full name: "$full_name"
            fi
        }
        #ueberpruefung auf die bestehenden gruppen
        function existing_usergroup () {
            if [ $(getent group $groupname) ]; then
                echo "Existing group: $groupname"
                directory_struc
            else
                echo "Didn't find group: $groupname"
                echo "New Group: $groupname"
                sudo groupadd $groupname
                User log: created $groupname
                directory_struc
            fi
        } 
        #ueberpruefung auf das bestehende home directory verzeichnis
        function existing_home_directory () {
            if [ ! -d "/home/$existing_username" ]; then
                echo "Existing Homedirectory for: $username"
            else
                echo "ERROR, not existing Home Directory for: $username"
                sudo mkdir -p /home/$username
                Homedirectory log 'for' existing User: created $username
            fi
        } existing_usergroup
        echo "FINISHED"
done < $filename