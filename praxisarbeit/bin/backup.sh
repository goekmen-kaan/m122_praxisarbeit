cwd=$(pwd)		# current working directory
cd $(dirname $0)	# change to the directory where the script is located
BINDIR=$(pwd)	# BINDIR: the directory where the script is located
cd $cwd		# return to the working directory
BASENAME=$(basename $0)	# Set the script name (without path to it)
TMPDIR=/tmp/$BASENAME.$$	# Set a temporary directory if needed
source ../etc/create_backup.sh
. functionbckp.env

touch userlist.txt;
cat $BCKPLOC/groups.env | grep -v '^#' | grep -v '^$' | while read groupname; 
do
    if [ $(getent group $groupname)]; then
        LOGLEV=I
        GID="$(getent group "$groupname" | cut -d: -f4)"
        echo "$GID" >> userlist.txt;
    else
        LOGLEV=W
        echo "Group $groupname does not exist."
    fi
done

awk '!seen[$0]++' userlist.txt > usersList.txt;
rm -r userlist.txt;

cat usersList.txt | grep -v '^#' | grep -v '^$' | while read user
do
    if [ -d "/home/def/$user"]; 
then
        homedir=$(getent passwd $user | cut -d: -f6)
        cp -r $homedir $TMPDIR
        awk -F: -v username=$user '$1==username /etc >> dirlist.txt'
    else
        LOGLEV=W
        echo "Directory /home/$user not found."
    fi
done

rm -r usersList.txt;

tar -cvf $BCKPLOC/backup.tar -T dirlist.txt
rm -r dirlist.txt;