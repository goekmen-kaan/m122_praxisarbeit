LOGLEV=I
log(){
    DATE=`date '+%Y.%m.%d %H:%M:%S'`
    LEVEL=$1
    shift
    case $LEVEL in
        D) 
            case $LOGLEV in
                D) 
                    echo "$DATE:DEBUG:$*"
                    ;;
                esac
                ;;
        I)
            case $LOGLEV in
                D|I)
                    echo "$DATE:INFO:$*"
                    ;;
                esac
                ;;
        W)
            case $LOGLEV in
                D|I|W)
                    echo "$DATE:WARNING:$*"
                    ;;
                esac
                ;;
        E)
            echo "$DATE:ERROR:$"
            ;;
        *)
            echo "$DATE:UNKNOWN:$*"
            ;;
    esac
}