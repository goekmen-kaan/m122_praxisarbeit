# Projekt Dokumentation

[[_TOC_]]

## Lösungsdesign
Anhand der Analyse wurde folgendes Lösungsdesign entworfen.

### Aufruf der Skripte

TODO: schreiben sie wie die Skripte aufgerufen werden sollen (d.h. welche Parameter werden übergeben, gibt es Interaktionen mit dem Skript, läuft es automatisch täglich ab?)

Das Ziel ist es ein sogenanntes Erfassungssystem zu erstellen, mit welchem neue Mitarbeiter einer Firma dem System hinzugefügt werden können.
Die User-Daten, sprich der Username, wie auch Default-Passwörter zu den Unix-Usern werden von einem separaten File entnommen und automatisch mit dem ersten Skript eingelesen / verarbeitet.

Dabei gibt es einige logische Einschränkungen und Überprüfungen, wie:
- Die Datei, in welcher alle wichtigen Daten des Unix-Users dargestellt werden, ist in einem bestimmten Format bzw. in einer bestimmten Reihenfolge angeordnet. Damit das Einlesen mit dem ersten Skript automatisch und erfolgreich funktioniert ist die Berücksichtigung des Formats/der Eingaben. sehr wichtig. (siehe hier: ![image-2.png](./image-2.png))
- Username muss UNIQUE sein, also darf im System nicht ein zweites Mal vorkommen.
- Für jeden neuen User wird ein User-Home-Verzeichnis (Mit einem Template namens Skeleton) erstellt.
    - Je nach Gruppe des Users (z.B Marketing) sind zusätzlich, spezifische Elemente und Dateien vorhanden, die sich von Gruppe zu Gruppe unterscheiden.
    - Die Gruppe, die dem ersten Skript mitgegeben wird, muss eine Gruppe sein, die im zweiten Skript gesichert wird.
        - Das User-Home-Verzeichnis wird
    - Jedes Verzeichnis hat bereits sogenannte Standard-Dateien (siehe unten), die schreibgeschützt und daher nicht veränderbar sein sollen: \
    ![image.png](./image.png)

Parameter
-----------------------------------------------------------------
Script 1: Folgende Parameter werden beim ersten Skript gebraucht: \
    ` <username>, <groupname>, <vorname nachname>  `

Script 2: Folgende Parameter werden beim zweiten Skript gebraucht: \
    `  <groupname1>, <groupname2>...   `


### Ablauf der Automation
UML zu Script 1:

![image-5.png](./image-5.png)

UML zu Script 2:

![image-6.png](./image-6.png)

### Konfigurationsdateien

TODO: Definieren sie welche Parameter in welchen Konfigurationsdateien gespeichert werden.

config_skr_1.env - Konfigurationsdatei bzg. Skript 1 \
defaultPw = "Default Passwort für jeden User - beim ersten Login ändern" \
roles = "Rollen, z.B Admin etc."

backup_config_skr_2.env - Konfigurationsdatei bzg. Skript 2 \
bckpLoc = "Location bzw. Speicherort des Backup-Files" \
bckpAnz = "Anzahl, wie oft das das Backup erstellt werden soll" \
bckpName = "Name des Backupfiles" 

## Abgrenzungen zum Lösungsdesign

TODO: Nachdem das Programm verwirklicht wurde, hier die Unterschiede von der Implementation zum Lösungsdesign beschreiben (was wurde anders gemacht, was wurde nicht gemacht, was wurde zusaetzlich gemacht)
