# Betriebsdokumentation
[[_TOC_]]
## Einführungstext 

TODO: In 2-3 Sätzen beschreiben was die Skripte grundsaetzlich tun.

Erstes Skript: Creating new User
Ein extra File, in welchen die User in einem einheitlichen Format abgespeichert werden, ist die Grundlage für dieses Skript. Mit dem ersten Skript werden diese Daten so verarbeitet, dass die neue User-Logins erstellt werden.

Zweites Skript: Creating backups
In einem separaten File werden sogenannte User-Groups abgelegt. Mit dem zweiten Skript können wir gezielte User-Directorys als Backup speichern/ablegen.

## Installationsanleitung für Administratoren

### Installation

TODO: Wie ist das Skript zu installieren. (z.B. apt-get install ... oder tar xvf .... oder ...)

Dieses Git-Repository ermöglicht uns eine einfache und gezielte Zusammenarbeit an den Dateien etc. Da alle Inhalte hier zentral gespeichert werden können wir ganz einfach auch auf dieses öffentliche Repository mit dem "Git clone" Befehl zugreifen bzw. herunterladen.

Befehl: git clone https://gitlab.com/goekmen-kaan/m122_praxisarbeit.git

Der User kann am gesamten Repo nun lokal arbeiten.

### Konfiguration

TODO: Beschreibung der Konfigurationsfiles (Beispiel-Files erstellen im Repo)

TODO: Wie ist ein allfaelliger Cronjob einzurichten

TODO: Wie sind User-Home-Templates einzurichten

##### Erstes Skript: Creating new user
Für das erste Skript muss zuerst noch einiges ausgeführt werden, bevor die Umgebung einwandfrei funktioniert. Da wir das User-Home-Directory mit dem Skeleton Template erstellen, werden sogenannte Skeleton Trees benötigt. 

Mit ganz einfachen Befehlen kann diese Einrichtung erfolgt werden.

Im Repository, im "bin" Ordner befindet sich ein File mit dem Namen "setup.sh". Mit den Berechtigungen, die noch zusätzlich angegeben werden müssen und dem setup File steht der Skript-Ausführung nichts im Wege.

Befehl: 
sudo chmod 755 setup.sh

##### Schritt 1 - Zum "skel" Verzeichnis navigieren.

Befehl: cd /etc/skel

##### Schritt 2 - Die Hauptverzeichnisse für die Gruppen erstellen

sudo mkdir -p hr

sudo mkdir -p testgroup1
 
sudo mkdir -p sudo

##### Schritt 3 - Customs Templates in die neu erstellten Ordner einrichten

Befehl 1: 
cd hr
sudo mkdir -p tmpdir/{Documents/sources/{includes,docs},Repo,tags}

Befehl 2:
cd testgroup1
sudo mkdir -p tmpdir/{Documents/sources/{includes,docs},Repo,tags}

Befehl 3:
cd sudo
sudo mkdir -p tmpdir/{Documents/sources/{includes,docs},Repo,tags}

##### Schritt 4 -  Rechte Vergabe (FILE-Rechte etc.)

Die Rechte müssen zusätzlich noch den Skripten einzeln gegeben werden, damit die Ausführung komplett einwandfrei funktioniert.

Zuerst dorthin navigieren, wo das Repository auch geclont wurde:

Befehl: cd /home/USER/REPO_PFAD/m122_praxisarbeit/praxisarbeit/bin

Dann den einzelnen Dateien/Skripten die Rechte geben:

Befehl 1: sudo chmod 755 create_users.sh
Befehl 2: sudo chmod 755 log.sh

## Bediensanleitung Benutzer

TODO: Erzeugen der Input-Files beschreiben, falls noetig

TODO: beschreiben des Scriptaufruf

TODO: beschreiben der erzeugt files (falls solche erzeugt werden)

TODO: Lokation von logfiles und bekannte Fehlermeldungen beschreiben.

##### Erstes Skript: Creating User Skript
Das erste Skript braucht eine separate Datei, um die User-Daten auszulesen. Hier ist es wichtig, dass das einheitliche Format eingehalten wird für alle User-Daten.

Mit den "username", "groupname" und "full_name" Variablen, die gebraucht werden, füllen wir diese Daten in dieser Reihenfolge ab.

In diesem Repository ist ein solches Input-File zum Auslesen bereits vorhanden und kann gebraucht werden: siehe /etc/users.env

Das Format, welches eingehalten und benutzt werden muss, nochmals hier klar dargestellt:

Für den ersten User z.B: username1 groupname1 name_firstname1 ...

##### Schritt 1 - Skript aufrufen und ausführen (Vorgabe: Man muss sich im Ordner "bin" befinden)

./create_users.sh -f FILE_FOR_USER_DATA.env (Dieses File muss im gleichen Ordner sein)

##### Schritt 2 - Skript für die Gruppen Backups

Zuerst werden hier Hauptgruppen/Verzeichnisse definiert, die ein Backup benötigen.

In diesem Repository sind es: hr, testgroup1, sudo (Dies kann man z.B, wie wir es auch gemacht haben in einem separaten File festlegen mit dem Namen "groups.env")

Befehl 1: mv groups.env /REPO_GIT_PFAD/m122_praxisarbeit/praxisarbeit/bin

Auch hier nochmals wichtig zu erwähnen: Damit dieser Befehl genau so funktionieren soll, muss "groups.env" im selben Ordner wie die Datei "create_users.sh" liegen.

